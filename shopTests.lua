EXPORT_ASSERT_TO_GLOBALS = true
require('luaunit')
require('shop')

TestMoney = {}
    function TestMoney:setUp()
        Shop.balance = {}
    end
    function TestMoney:tearDown()
        Shop.balance = {}
    end
    function TestMoney:testGetBalance_returns0ForNew()
        assertEquals(Shop:getBalance('newUser'), 0)
    end
    function TestMoney:testAddMoney_getBalance()
        Shop:addMoney('Szel', 100)
        assertEquals(Shop:getBalance('Szel'), 100)
    end
    function TestMoney:testAddMoney_negativeAmount()
        Shop:addMoney('Tig', 80)
        Shop:addMoney('Tig', -20)
        assertEquals(Shop:getBalance('Tig'), 60)
    end
    function TestMoney:testAddMoney_returnsTrue()
        assertTrue(Shop:addMoney('User', 100))
    end
    function TestMoney:testAddMoney_returnsFalse()
        assertFalse(Shop:addMoney('User', -100))
    end
    function TestMoney:testCanAfford_canAfford()
        Shop:addMoney('User', 50)
        assertTrue(Shop:canAfford('User', 5))
    end
    function TestMoney:testCanAfford_cannotAfford()
        Shop:addMoney('User', 50)
        assertFalse(Shop:canAfford('User', 55))
    end

TestItems = {}
    function TestItems:setUp()
        Shop.items = {}
    end
    function TestItems:tearDown()
        Shop.items = {}
    end
    function TestItems:testSetItems_getItem()
        local items = {
            {name = 'hat1', price = 10},
            {name = 'hat2', price = 30}
        }
        Shop:setItems(items)
        local item = Shop:getItem('hat1')
        assertEquals(item.name, 'hat1')
        assertEquals(item.price, 10)
    end
    function TestItems:testSetItems_replaceOld()
        local items = {
            {name = 'hat1', price = 10},
            {name = 'hat2', price = 30}
        }
        local otherItems = {
            {name = 'hat3', price = 66},
            {name = 'hat2', price = 32}
        }
        Shop:setItems(items)
        Shop:setItems(otherItems)
        assertNotNil(Shop:getItem('hat3'))
        assertNil(Shop:getItem('hat1'))
        assertEquals(Shop:getItem('hat2').price, 32)
    end
    function TestItems:testGetItem_returnsNil()
        assertNil(Shop:getItem('someItem'))
    end
    function TestItems:testAddItem()
        local item = {name = 'hat', price = 55}
        Shop:addItem(item)
        local rItem = Shop:getItem('hat')
        assertNotNil(rItem)
        assertEquals(rItem.price, 55)
    end
    function TestItems:testAddItem_replacesOld()
        local item1 = {name = 'hat', price = 10}
        local item2 = {name = 'hat', price = 20}
        Shop:addItem(item1)
        Shop:addItem(item2)
        assertEquals(Shop:getItem('hat').price, 20)
    end
    function TestItems:testRemoveItem()
        local item = {name = 'hat', price = 22}
        Shop:addItem(item)
        Shop:removeItem('hat')
        assertNil(Shop:getItem('hat'))
    end
    
TestBuying = {}
    function TestBuying:setUp()
        Shop:addMoney('player1', 100)
        Shop:addItem({name = 'Hat', price = 40})
        Shop:addItem({name = 'Expensive hat', price = 250})
    end
    function TestBuying:tearDown()
        Shop.balance = {}
        Shop.items = {}
        Shop.playerItems = {}
    end
    function TestBuying:testBuy_tookFunds()
        assertTrue(Shop:buy('player1', 'Hat'))
        assertEquals(Shop:getBalance('player1'), 60)
    end
    function TestBuying:testBuy_hasItem()
        Shop:buy('player1', 'Hat')
        assertTrue(Shop:hasItem('player1', 'Hat'))
    end
    function TestBuying:testBuy_notEnoughFunds()
        assertFalse(Shop:buy('player1', 'Expensive hat'))
        assertFalse(Shop:hasItem('player1', 'Expensive hat'))
    end
    function TestBuying:testBuy_itemDoesntExists()
        assertFalse(Shop:buy('player1', 'void'))
    end
    function TestBuying:testGetPlayerItems()
        Shop:buy('player1', 'Hat')
        local items = Shop:getPlayerItems('player1')
        assertNotNil(items['Hat'])
    end
    function TestBuying:testGetPlayerItems_returnsTable()
        assertIsTable(Shop:getPlayerItems('someGuy'))
    end
    function TestBuying:testRemovePlayerItem()
        Shop:buy('player1', 'Hat')
        Shop:removePlayerItem('player1', 'Hat')
        assertFalse(Shop:hasItem('player1', 'Hat'))
    end
    function TestBuying:testRemovePlayerItem_passWhenHaveNoItem()
        Shop:removePlayerItem('player1', 'money')
    end
        
lu = LuaUnit.new()
lu:setOutputType("tap")
os.exit(lu:runSuite())