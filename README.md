# Documentation

## Money
Shop:getBalance(name)
---------------------
returns current balance for `name`
 
Shop:addMoney(name, amount)
---------------------------
adds `amount` to `name`s balance
`amount` can be negative
returns `true` if success and `false` when not enough money (when `amount` is negative)
 
Shop:canAfford(name, amount)
----------------------------
Checks whether `name` can buy something
returns `true` if can


## Shop items
Item is an object (table) which contain *must* contain `name` and `price` properties. 
`name` *must* be unique.
Example:

    item1 = {
        name = 'Scary hat`,
        price = 44    
    } 
    
Can contain also other properties not used by shop framework but yours script/minigame

    item2 = {
        name = 'Powerup hat',
        price = 99,
        image = 'G7B07fO.jpg',
        superJump = true        
    }  

Shop:setItems(items)
--------------------
Where `items` is table of items.
Replaces all shop items with new. Use when you are initiate script.
Example:

    items = {
        {
            name = "Pink cannon",
            price = 0
        },
        {
            name = "Blue cannon",
            price = 10
        },
            name = "Green cannon",
            price = 15
        }
    }
    
    Shop:setItems(items)
    
Shop:addItem(item)
------------------
Adds item to shop. If item with same name exists in shop then is replaced.

Shop:removeItem(itemName)
---------------------
Removes item from shop. Does not refund lost money for purchased item.

Shop:getItem(itemName)
----------------------
Returns table with item properties.
Note that lua passes table by reference so when you change something in returned table - like price - change will occur in shop too.

## Buying, player's items

Shop:buy(playerName, itemName)
------------------------------
Returns `false` if not succeed (probably not enough funds).

Shop:hasItem(playerName, itemName)
----------------------------------
Returns `true` if player has item, `false` otherwise

Shop:removePlayerItem(playerName, itemName)
-------------------------------------------
Removes item from player. Doesn't refund.

Shop:getPlayerItems(playerName)
-------------------------------
Returns table with player's items.