Shop = {}
    Shop.balance = {}
    function Shop:getBalance(name)
        return self.balance[name] or 0
    end
    function Shop:addMoney(name, amount)
        if amount < 0 and self:getBalance(name) + amount < 0 then
            return false
        end
        self.balance[name] = (self.balance[name] or 0) + amount
        return true
    end
    function Shop:canAfford(name, amount)
        return self:getBalance(name) >= amount
    end

    Shop.items = {}
    function Shop:setItems(items)
        Shop.items = {}
        for _,item in ipairs(items) do
            Shop.items[item.name] = item
        end
    end
    function Shop:getItem(itemName)
        return Shop.items[itemName]
    end
    function Shop:addItem(item)
        Shop.items[item.name] = item
    end
    function Shop:removeItem(itemName)
        Shop.items[itemName] = nil
    end
    
    Shop.playersItems = {}
    function Shop:buy(playerName, itemName)
        local item = self:getItem(itemName)
        if item and self:canAfford(playerName, item.price) then
           self:addMoney(playerName, -1 * item.price)
           self:getPlayerItems(playerName)[itemName] = true
           return true
        end
        return false
    end
    function Shop:hasItem(playerName, itemName)
        return self:getPlayerItems(playerName)[itemName] or false
    end
    function Shop:getPlayerItems(name)
        if not self.playersItems[name] then
            self.playersItems[name] = {}
        end
        return self.playersItems[name]
    end
    function Shop:removePlayerItem(playerName, itemName)
        self:getPlayerItems(playerName)[itemName] = nil
    end